var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var authentication = require('./middlewares/authentication');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger-definitions/swagger.json');

var appRoutes = require('./routes/v1/app');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// CORS
app.use(function(req, res, next){
	res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, PATCH, DELETE, OPTIONS');
  next();
});

// Authentication middleware
app.use('/api',authentication.validate);

// Define routes
app.use('/api/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/v1', appRoutes);
app.get('/api/', function(req, res) {
  res.render('index',{title:'SDR'});
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  
  var err = new Error('404 Not Found');
  err.status = 404;
  next(err);
});

// Define middleware to error handler
app.use(function(err, req, res, next) {
  if(!err.status)
    err.status = 404;
  var error = new Error();
  error.status = err.status;
  error.message = err.message;
  error.error = (err.error || '');
  res.status(error.status).send({status: 'error', message: (error.message || 'Internal error'), error: error.error});
});


// error handler
/*
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
*/


module.exports = app;
