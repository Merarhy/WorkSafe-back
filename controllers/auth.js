'use strict'

//var GoogleAuth = require('google-auth-library');
var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var getSlug = require('speakingurl');
var jwt = require('../services/jwt');
var config = require('config');
var mailing = require('../services/mailing');

/***************************************
*
*
*	Auth functions
*
*
***************************************/

exports.doLogin = function(req,res,next){
	/**	
	* Return a list of user permisions
	*
	* returns List
	**/

	var params = req.body;

	if(params.email && params.password){
		var populate = [
			{path : 'business'}
		];
		User.findOne({email:params.email}).populate(populate).exec( (err,user) => {
			
			if(err || !user || !user.password || !bcrypt.compareSync(params.password, user.password)){
				// User not foundm find business user
				
			} else {
				
				// Remove unnecesary vars
				var token_data = {
					_id: user._id,
					email: user.email,
					role: user.role._id,
					created_at: user.created_at
				}
				
				var tmp_user = JSON.parse(JSON.stringify(user));
				delete tmp_user.role.permissions;
				
				res.send({
					user: tmp_user,
					token:jwt.createToken(token_data)
					//permissions:user.role.permissions.map( (el) => {return el.slug; } )
				});
				
			}
			
		});
	} else {
		next({status: 200, message: 'Authentication data required.'});
	}

	
}

	


exports.validateToken = function(req,res,next){
	
	// If auth header is not present, then return error
	if(!req.headers.authorization){
		res.send({token:'',valid:false});
	}
	// get token from header
	var token = req.headers.authorization.replace(/['"]+/g, '');
	try{
		
		getUserByToken( token ).then( data => {
			res.send(data);
		}).catch( err => {
			res.send(err);
		});
		
	} catch(ex) {
		// If token is not valid returns error
		//console.log(ex)
		res.send({token:token,valid:false,status:'4'});
	}
}




var getUserByToken = exports.getUserByToken = function( token ){
	return new Promise( function (resolve, reject) {
		// Decode token
		var payload = jwt.decodeToken(token);
		// Compare token data... role, permissions, etc
		if(jwt.validateExpiration(payload.exp) && jwt.validateKey(payload.secret_key)){
			// Validate if is business user
			// Validate if is admin user
				User.findById(payload._id)
				.populate({path : 'role', populate : {path : 'permissions', select : 'slug'}})
				.exec( (err,userdata) => {
					if(err){
						reject({token:token,valid:false,status:'1'});
					} else {
						User.findById(payload._id).exec( (err,user) => {
				
							if(err || !user){
								reject({token:token,valid:false,status:'2'});
							} else {
								resolve({
									user: user,
									token:token,
									permissions:userdata.role.permissions.map( (el) => {return el.slug; } ),
									valid:true
								});
							}
							
						});
					}
				});
			
		} else {
			reject({token:token,valid:false,status:'3'});
		}
	});
}




exports.forgotPassword = function(req, res, next){




	var params =   req.body;
	
	
	if(params.email ){
		var populate = [
			{ path : 'user' }
	];

	
	
		User.findOne({email: params.email}).populate(populate).exec( (err,user) => {
				
			if(err || !user ){
				// User not foundm find business user
				
			} else {
				
				// Forgot password SDR
			
					var moment = require('moment');
					var token_encoded = jwt.createCustomToken({_id: user._id, exp: moment().add(1,'days').unix()});
					var options = {
						subject: 'Solicitud de restauracion de contraseña',
						recipients: {},
						template: 'resetPassword'
					}
					options.recipients[user.email] = {
								'token': token_encoded,
								'name': user.firstname+' '+user.lastname
							};
						mailing.send(options);
						res.send({message: 'Se ha enviado un correo en tu cuenta'});;
			}
			
		});

	}else{
		res.send({message: 'Field "email" is required'});
	}

}

exports.resetPassword = function(req,res,next){
	/**
	* Reset password
	*
	* returns token
	**/

	var params = req.body;

	var token_data = jwt.decodeToken(params.token);
	// Validate token ID and expiration
	if(!token_data._id || !jwt.validateExpiration(token_data.exp)){
		next({status: 200, message: 'Error retriving token data.'});
	} else {
		var user = {};
		user.password = bcrypt.hashSync(params.password);
		
		// First find if user exists
		User.findById(token_data._id).exec((err, userFind) => {
			if(err){
				next({status: 200, message: 'User doesnt exists.'});
			}else{
				if(userFind){
					User.findByIdAndUpdate(token_data._id, {$set: user}, {new: true} ).exec((err, user) => {
						if(err){
							next({status: 200, message: 'Error updating user.'});
						} else {
							if(user)
								res.send(user);
							else{
								next({status: 200, message: 'Error updating user.'});
							}
						}
					});
				}else{
					next({status: 200, message: 'Error retriving user.'});
				}
			}

		});	
	}
	
}

exports.resetPasswordRequest = function(req,res,next){
	/**
	* Return a list of user permisions
	*
	* returns List
	**/

	var params = req.body;

	var token_data = jwt.decodeToken(req.params.token);

	// Validate token ID and expiration
	if(!token_data._id || !jwt.validateExpiration(token_data.exp)){
		next({status: 200, message: 'Error retriving token data.'});
	} else {
		
				User.findById(token_data._id).exec((err, user) => {
					if(err){
						next({status: 200, message: 'User doesnt exists.'});
					}else{
						if(user){
							res.send(user);
						}else{
							next({status: 200, message: 'Error retriving token data.'});
						}
					}
				});
		
	}

}

