'use strict'

var fs = require('fs');
var path = require('path');
var bcrypt = require('bcrypt-nodejs');
var Directory = require("../models/file").Directory;
var File = require("../models/file").File;
var Prueba = require("../models/file").Prueba;
var getSlug = require('speakingurl');
var config = require('config');

/***************************************
*
*
*	Directories function
*
*
***************************************/



exports.getDirectoriesByIdBusiness = function(req,res){
	/**
	* Return a list of business directories
	*
	* returns List
	**/
	if(req.user.business && req.user.business._id){
		var businessid =  req.user.business._id; 
	} else {
		var businessid =  req.params.businessid; 
	}

	Directory.find({_business: businessid}).lean().exec(function(err, data){



		res.send({data: data});
	});
}

function sortDirectories(dirs,parent){
	
	if(!elements)
		var elements = [];
	if(parent){
		elements = dirs.filter((el)=>{ return String(parent)==String(el._parent) });
	}else{
		elements = dirs.filter((el)=>{ return !el._parent });
	}

	elements.forEach((item, index)=>{
		elements[index].children = sortDirectories(dirs,item._id);
	});
	return elements;
}

exports.addDirectoryByIdBusiness = function(req,res,next){
	/**
	* Creates a new business directory.
	*
	* name Directory 
	* returns Directory
	**/




	if(req.user.business && req.user.business._id){
		var businessid =  req.user.business._id; 
	} else {
		var businessid =  req.params.businessid; 
	}

	var directory = new Directory();
	var params = req.body;
	if(!params.name)
		res.send({message: 'Field "name" is required'});
	directory.name = params.name;
	if(params._parent)
		directory._parent = params._parent;
	directory._business = businessid;
		
	directory.owner = req.user._id;

	directory.save( (err, directory) => {
		if(err){
			next({status: 200, message: 'Error saving directory.'});
			console.log(err);
		} else {
			if(directory){
				res.send(directory);
			}else{
				next({status: 200, message: 'Error saving directory.'});
			}
		}
	} );
	
}


exports.deleteDirectoryByIdBusinessById = function(req,res,next){
	/**
	* Delete directory.
	*
	* id Long ID of the directory to delete
	* no response value expected for this operation
	**/

	if(req.user.business && req.user.business._id){
		var businessid =  req.user.business._id; 
	} else {
		var businessid =  req.params.businessid; 
	}



	
	Directory.remove({_business:businessid,_id:req.params.id}, (err, directoryRemoved) => {
		if(err){
			next({status: 200, message: 'Directory doesnt exists.'});
		} else {
			if(directoryRemoved){
				Directory.update(
				   { _parent: req.params.id },
				   { $unset: { _parent: "" } },
				   function(){
				   		res.send(directoryRemoved);
				   }
				);
			}else{
				next({status: 200, message: 'Error deleting directory.'});
			}
		}
	});
	
}

exports.getsdrFavDirectoriesByIdBusiness = function(req,res,next){

	if(req.user.business && req.user.business._id){
		var businessid =  req.user.business._id; 
	} else {
		next({status: 200, message: 'User not authorized.'});
		return;
	}

	return new Promise( (resolve, reject) => {
		
		var files_promises = [];
		
		files_promises.push( File.find({_business:businessid}).lean().exec() );
		files_promises.push( Directory.find({_business: businessid}).lean().exec() );

		Promise.all(files_promises).then( files_responses => {
			// Create empty root directory

			files_responses[1] = files_responses[1].filter( el => el.favorite );

			var dir_ids = files_responses[1].map( el => String(el._id) );

			files_responses[0] = files_responses[0].filter( el => (el._directory && dir_ids.indexOf( String(el._directory) )!=-1) );
			
			files_responses[0].map( (file,index) => {
				files_responses[1] = files_responses[1].map( (dir,index) => {
					if ( String(dir._id) == String(file._directory) ){
						if ( !dir.files ) {
							dir.files = [];
						}
						dir.files.push( file );
					}
					return dir;
				});
			});
			
			
			res.send( files_responses[1] );
		}).catch( err => {
			console.log( err );
			next({status: 200, message: 'Error loading directories.'});
		});

	});

}

exports.sdrfavDirectoryByIdBusinessById = function(req,res,next){

	var businessid =  req.params.businessid; 

	var directory = {
		favorite: true
	};

	Directory.findOneAndUpdate({_business:businessid,_id:req.params.id},{$set: directory}, {new: true}).exec( (err, directoryUpdated) => {
		if(err){
			next({status: 200, message: 'Directory doesnt exists.'});
		} else {
			if(directoryUpdated){
				res.send(directoryUpdated);
			}else{
				next({status: 200, message: 'Error updating directory.'});
			}
		}
	});
}

exports.sdrunfavDirectoryByIdBusinessById = function(req,res,next){

	var businessid =  req.params.businessid; 
	
	var directory = {
		favorite: false
	};

	Directory.findOneAndUpdate({_business:businessid,_id:req.params.id},{$set: directory}, {new: true}).exec( (err, directoryUpdated) => {
		if(err){
			next({status: 200, message: 'Directory doesnt exists.'});
		} else {
			if(directoryUpdated){
				res.send(directoryUpdated);
			}else{
				next({status: 200, message: 'Error updating directory.'});
			}
		}
	});
}










/***************************************
*
*
*	Files functions
*
*
***************************************/

exports.getBusinessFilesById = function(req,res){
	/**
	* Return a list of business files
	*
	* returns List
	**/

	if(req.user.business && req.user.business._id){
		var businessid =  req.user.business._id; 
	} else {
		var businessid =  req.params.businessid; 
	}

	File.find({_business:businessid}).exec(function(err, data){
		res.send({data: data});
	});
}

exports.addBusinessFile = function(req,res,next){
	/**
	* Creates a new business file.
	*
	* name File 
	* returns File
	**/

	var filename = 'Upload error...';
	if(req.user.business && req.user.business._id){
		var businessid =  req.user.business._id; 
	} else {
		var businessid =  req.params.businessid; 
	}

	if(req.files){
		var file_path = req.files.bus_file.path;

		if(config.upload_types.indexOf(req.files.bus_file.type) !== -1){
			
			var file = new File();
			var params = req.body;
			var filepath = config.upload_dir.replace("./","");
			file.name = req.files.bus_file.name;
			file.file = req.files.bus_file.path.replace(filepath,"");
			file.mime = req.files.bus_file.type;
			file.owner = req.files.owner;
			file._business = businessid;
			file.owner = req.user._id;
			file.size = req.files.bus_file.size;
			if(file.mime == 'image/jpeg' || file.mime == 'image/png'){
				file.type = 'image'
			}else{

				file.type = 'document';
 
			}

			if(req.params.directoryid!='0')
				file._directory = req.params.directoryid;

			

			file.save( (err, file) => {
				if(err){
					//fs.unlink(config.upload_dir+file.file);
					console.log(err);
					next({status: 200, message: 'Error saving file.'});
				} else {
					if(file)
						res.send(file);
					else{
						//fs.unlink(config.upload_dir+file.file);
						next({status: 200, message: 'Error saving file.'});
					}
				}
			} );
			
		} else {
			fs.unlink("./"+req.files.bus_file.path);
			next({status: 200, message: 'Not support for file format: '+req.files.bus_file.type});
		}
	} else {
		next({status: 200, message: 'File is required.'});
	}
	
}


exports.deleteBusinessFileById = function(req,res,next){
	/**
	* Delete file.
	*
	* id Long ID of the file to delete
	* no response value expected for this operation
	**/
	
	var file = config.upload_dir+req.params.file
	if(req.user.business && req.user.business._id){
		var businessid =  req.user.business._id; 
	} else {
		var businessid =  req.params.businessid; 
	}
	
				
	File.remove({_business:businessid,file:req.params.file}, (err, fileRemoved) => {
		if(err){
			next({status: 200, message: 'File doesnt exists.'});
		} else {
			if(fileRemoved && fileRemoved.n!=0){
				fs.exists(file, function(exists){
					if(exists){
						fs.unlink(file);
						res.send(fileRemoved);
					} else {
						res.send(fileRemoved);
					}
				});
			}else{
				next({status: 200, message: 'Error deleting file.'});
			}
		}
	});
			
	
	
}

exports.serveBusinessFileById = function(req,res,next){
	var file = config.upload_dir+req.params.file
	if(req.user && req.user.business && req.user.business._id){
		var businessid =  req.user.business._id; 
	} else {
		var businessid =  req.params.businessid; 
	}

	File.findOne({_business:businessid,file:req.params.file}, (err, fileServe) => {
		if(err){
			next({status: 200, message: 'File doesnt exists.'});
		} else {
			if(fileServe && fileServe.file){
				fs.exists(file, function(exists){
					if(exists){
						res.sendFile(path.resolve(file));
					} else {
						next({status: 200, message: 'File unavailable.'});
					}
				});
			}else{
				next({status: 200, message: 'File unavailable.'});
			}
		}
	});
}

exports.serveAvatarFileByName = function(req,res,next){
	var file = config.upload_avatar_dir+req.params.file
	fs.exists(file, function(exists){
		if(exists){
			res.sendFile(path.resolve(file));
		} else {
			next({status: 200, message: 'File unavailable.'});
		}
	});
}

