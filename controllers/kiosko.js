'use strict'
var fs = require("fs");
var path = require('path');
var config = require('config');
var bcrypt = require('bcrypt-nodejs');
var getSlug = require('speakingurl');

var UserRole = require('../models/user_role');
var Categories = require("../models/settings").Categorie;
var Kiosko = require("../models/kiosko").Kiosko;







/***************************************
*
*
*	KIOSKO FUCTION
*
*
***************************************/



exports.getKioskoCatalogos = function(req,res){

    Categories.find({}).exec(function(err, data){
		res.send({data: data});
	});

}


/***************************************
*
*
*	CATALOGOS FUNCTION
*
*
***************************************/


exports.getSitestoCatalogById = function(req, res, next){

	var sites = {
		_categorie: req.params.catalogoid
	} 

	Kiosko.find(sites).exec(function (err, data){
		if(err){
			next({status: 200, message: 'Error.'});
		}else{
			
			res.send(data);
		}	
	});

}

exports.getSiteById = function(req, res, next){


	Kiosko.findOne({_id: req.params.siteid}, function(err, site){
		if(err){
			console.log(err)
			next({status: 200, message: 'Error.'});
		}else{
			console.log(site);
			res.send(site);
		}
   });

 
}



exports.addSitetoCatalogById = function(req, res, next){
	
	var filename = 'Upload error...';

	if (req.params.catalogoid){

		if(req.files){

			var file_path = req.files.bus_file.path;

			if(config.upload_types.indexOf(req.files.bus_file.type) !== -1){
					
					var params =  JSON.parse(req.body.bus);
					var kiosko = new Kiosko(); 

					// Params
					kiosko._categorie = req.params.catalogoid;

					// Body
					if(!params.name)
						res.send({message: 'Field "nombre" is required'});
					if( !Number(params.lng) && isNaN(Number(params.lng)) === true )
						res.send({message: 'Field "longitud" is required and is type number'});
					if( !Number(params.lng) && isNaN(Number(params.lat)) === true )
						res.send({message: 'Field "latitud" is required and is type number'});
					if(!params.colonia)
						res.send({message: 'Field "colonia" is required'});
					if(!params.municipio)
						res.send({message: 'Field "municipio" is required'});
					if(!params.calle)
						res.send({message: 'Field "calle" is required'});
					if(!params.estado)
						res.send({message: 'Field "estado" is required'});
					if(!params.tel)
						res.send({message: 'Field "telefono" is required'});
					if(!params.description)
						res.send({message: 'Field "descripción" is required'});
				
					

					kiosko.name = params.name;
					kiosko.lng = Number(params.lng);
					kiosko.lat = Number(params.lat);
					kiosko.colonia = params.colonia;
					kiosko.municipio = params.municipio;
					kiosko.calle = params.calle;
					kiosko.estado = params.estado;
					kiosko.tel = params.tel; 
					kiosko.description = params.description;
					
					// File
					var filepath    = config.upload_site.replace("./","");
					kiosko.img  = {
						name: req.files.bus_file.name,
						file: req.files.bus_file.path.replace(filepath,""),
						type: req.files.bus_file.type,
						size:  req.files.bus_file.size
					} 
				


					kiosko.save( (err, kiosko) => {
						if(err){
							next({status: 200, message: 'Error saving site.'});

						} else {
							if(kiosko){

								res.send(kiosko);
								
							}else{
								next({status: 200, message: 'Error saving site.'});
							}
						}
					} );
			}else{
				fs.unlink("./"+req.files.bus_file.path);
				next({status: 200, message: 'Not support for file format: '+req.files.bus_file.type});
			}
		} else{
			
			next({status: 200, message: 'No audio files received.'});	
		
		}	
	}else{
		next({status: 200, message: 'No catalog ID received.'});
	}
	
}

exports.editSiteToCatalogById = function(req, res, next){



		var params =  JSON.parse(req.body.bus);
		var kiosko = {};


		kiosko.name = params.name;
		kiosko.lng = Number(params.lng);
		kiosko.lat = Number(params.lat);
		kiosko.colonia = params.colonia;
		kiosko.municipio = params.municipio;
		kiosko.calle = params.calle;
		kiosko.estado = params.estado;
		kiosko.tel = params.tel; 
		kiosko.description = params.description;
		
		console.log();
		
		if (req.files.bus_file){
			var filepath    = config.upload_site.replace("./","");
			kiosko.img  = {
				name: req.files.bus_file.name,
				file: req.files.bus_file.path.replace(filepath,""),
				type: req.files.bus_file.type,
				size:  req.files.bus_file.size
			}
		}
		
	
		Kiosko.findByIdAndUpdate({_id: params._id}, {$set: kiosko}, {new: true} ).exec((err, site)=> 	{
			if(err){
				next({status: 200, message: 'Site doesnt exists.'});
				console.log(err);

			}else{
				if(site){
					res.send(site);
				}else{
					next({status: 200, message: 'Error updating site.'});
				}
			}

		});
				
}


exports.deleteSiteFromCatalog = function(req, res, next){

	var sites= {
		_id: req.params.siteid
	} 

	Kiosko.remove(sites, (err, response) => {
		if(err){
			next({status: 200, message: 'Error remove Sites.'});
		}else{
			res.send(response)
		}
	});

}

exports.serverSiteImgId = function(req, res, next){

	

	var file = config.upload_site+req.params.img;
	
	fs.exists(file, function(exists){
		if(exists){
			res.sendFile(path.resolve(file));
			} else {
			next({status: 200, message: 'File unavailable.'});
		}
	});

				
}
