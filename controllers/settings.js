'use strict'
var Option = require("../models/settings").Option;
var common = require('../services/common');
var crypto = require('../services/crypto');

/***************************************
*
*
*	Options function
*
*
***************************************/

exports.getOptions = function(req,res,next){
	/**
	* Return a list of site options
	*
	* returns List
	**/
	Option.find({},{'__v':0}).exec(function(err,data){
		res.send({data:data});
	})
	
}

exports.addOption = function(req,res,next){
	/**
	* Return a new option item
	*
	* returns Option
	**/
	
	var item = new Option();

	for(var key in req.body){
		item[key] = req.body[key];
	}
	
	item.save( (err,data) => {
		if(err || !data){
			next({status: 200, message: 'Error saving option.'});
		} else {
			res.send(data);
		}
	})
	
}

exports.updateOptionById = function(req,res,next){
	/**
	* Update option item.
	*
	* returns Option
	**/

	var item = {};
	
	for(var key in req.body){
		if(key!=='_id')
			item[key] = req.body[key];
	}

	Option.findByIdAndUpdate(req.params.id, item, {new: true}).exec(function(err, data){
		if(err || !data){
			next({status: 200, message: 'Error updating option.', error: common._handleError(err)});
		} else {
			res.send(data);
		}
	});
	
}

exports.deleteOptionById = function(req,res,next){
	/**
	* Return a option deleted status
	*
	* returns Deleted option
	**/

	Option.findByIdAndRemove(req.params.id).exec(function(err,data){
		if(err || !data){
			next({status: 200, message: 'Error deleting option.', error: common._handleError(err)});
		} else {
			res.send(data);
		}
	})
	
}


