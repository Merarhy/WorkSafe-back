'use strict'

var common = require('../services/common');
var config = require('config');


/***************************************
*
*
*	Testing function
*
*
***************************************/

// Get IP location
/*
exports.test = function(req,res){
	var geoip = require('geoip-lite');
	var ip = "10.50.11.153";
	var geo = geoip.lookup(ip);
	res.send(geo);
}
*/

// Send email test
exports.test = function(req,res,next){
	
	
	var SSH = require('simple-ssh');
	var ssh = new SSH({
		port: config.pbx.port,
		host: config.pbx.host,
		user: config.pbx.username,
		pass: config.pbx.password
	});

	ssh.on('error', function(err) {
		console.log('Oops, something went wrong.');
		console.log(err);
		ssh.end();
		next({status: 200, message: 'Error connecting ssh.', error: err});	
	});

	ssh.exec("perl /usr/local/bin/dial_alert_grab_sare.pl 910457225124721 1234567890", {
		out: function(stdout) {
			ssh.end();
			console.log('SSH end');
			res.send({data: stdout});
		},
		exit: function(code) {
			ssh.end();
			console.log('SSH error dial');
			res.send({data: code});
		}
	})
	.start();

	
	ssh.on('ready', function(err) {
		
		console.log('SSH ready');
		ssh.exec('ls -la', {
			out: function(stdout) {
				ssh.end();
				console.log('SSH end');
				res.send({data: stdout});
			},
			exit: function(code) {
				ssh.end();
				console.log('SSH error');
				res.send({data: code});
			}
		})
		
	});
	
	
	
	
	
	/*** Using the `args` options instead ***/
	/*
	ssh.exec('echo', {
		args: ['$PATH'],
		out: function(stdout) {
			console.log(stdout);
		}
	}).start();
	*/
	
}

let available_collections = [
	'Voltage',
	'Sts',
    'RegletaPdu',
    'ReceptaclePdu',
    'Wire',
	'Carrier',
	'Rack',
	'Charola',
	'Organizador',
	'Pasacable',
    'Datacenter',
    'SalesforceProduct',
	'Option'
]
/***************************************
*
*
*	Catalogs function
*
*
***************************************/

exports.getCollection = function(req,res,next){
	/**
	* Return a list of collection items
	*
	* returns List
	**/
	var Collection = require("../models/"+req.params.type)[req.params.collection];
	Collection.find({},{'__v':0}).exec(function(err,data){
		res.send({data:data});
	});
	
}

exports.addCollection = function(req,res,next){
	/**
	* Return a new collection item
	*
	* returns Item
	**/
		
	var Collection = require("../models/"+req.params.type)[req.params.collection];
	var item = new Collection();

	for(var key in req.body){
		item[key] = req.body[key];
	}
	
	
	item.save( (err,data) => {
		if(err || !data){
			console.log(err);
			next({status: 200, message: 'Error saving item.'});
		} else {
			res.send(data);
		}
	})
	
}

exports.updateCollectionById = function(req,res,next){
	/**
	* Update collection item.
	*
	* returns Item
	**/

	var Collection = require("../models/"+req.params.type)[req.params.collection];

	var item = {};
	
	for(var key in req.body){
		if(key!=='_id')
			item[key] = req.body[key];
	}

	Collection.findByIdAndUpdate(req.params.id, item, {new: true}).exec(function(err, data){
		if(err || !data){
			next({status: 200, message: 'Error updating item.', error: common._handleError(err)});
		} else {
			res.send(data);
		}
	});
	
}

exports.deleteCollectionById = function(req,res,next){
	/**
	* Return a item deleted status
	*
	* returns Deleted status
	**/
	
	var Collection = require("../models/"+req.params.type)[req.params.collection];

	Collection.findByIdAndRemove(req.params.id).exec(function(err,data){
		if(err || !data){
			next({status: 200, message: 'Error deleting item.', error: common._handleError(err)});
		} else {
			res.send(data);
		}
	})
	
}


/***************************************
*
*
*	Mobile functions
*
*
***************************************/
exports.registerDeviceToken = function(req,res,next){
	/**
	* Register device token for push notifications
	*
	* returns token
	**/
	var token_exists = req.user.device_tokens.find( token => token===req.params.token );
	if( token_exists ) {
		res.send({token: req.params.token, status: 'already exists'});
		return;
	}
	// Register business user device token
	if( req.user.business ) {
		var BusinessUser = require('../models/business_user');
		BusinessUser.findByIdAndUpdate(req.user._id, {$push: {"device_tokens": req.params.token}}, {new: true} ).exec((err, user) => {
			if(err){
				next({status: 200, message: 'Error al agregar el dispositivo.'});
			} else {
				if(user)
					res.send({token: req.params.token, status: 'created' });
				else{
					next({status: 200, message: 'Error al agregar el dispositivo.'});
				}
			}
		});
	}
	// Register kio user device token
	if( !req.user.business ) {
		var User = require('../models/user');
		User.findByIdAndUpdate(req.user._id, {$push: {"device_tokens": req.params.token}}, {new: true} ).exec((err, user) => {
			if(err){
				next({status: 200, message: 'Error al agregar el dispositivo.'});
			} else {
				if(user)
					res.send({token: req.params.token, status: 'created' });
				else{
					next({status: 200, message: 'Error al agregar el dispositivo.'});
				}
			}
		});

	}	
	
}

exports.deleteDeviceToken = function(req,res,next){
	/**
	* Remove device token for push notifications
	*
	* returns token
	**/
	
	var token_exists = req.user.device_tokens.find( token => token===req.params.token );
	if( !token_exists ) {
		res.send({token: req.params.token, status: 'token doesnt exists'});
		return;
	}
	// Register business user device token
	if( req.user.business ) {
		var BusinessUser = require('../models/business_user');
		BusinessUser.findByIdAndUpdate(req.user._id, {$pull: {"device_tokens": req.params.token}}, {new: true} ).exec((err, user) => {
			if(err){
				next({status: 200, message: 'Error al borrar el dispositivo.'});
			} else {
				if(user)
					res.send({token: req.params.token, status: 'deleted' });
				else{
					next({status: 200, message: 'Error al borrar el dispositivo.'});
				}
			}
		});
	}
	// Register kio user device token
	if( !req.user.business ) {
		var User = require('../models/user');
		User.findByIdAndUpdate(req.user._id, {$pull: {"device_tokens": req.params.token}}, {new: true} ).exec((err, user) => {
			if(err){
				next({status: 200, message: 'Error al borrar el dispositivo.'});
			} else {
				if(user)
					res.send({token: req.params.token, status: 'deleted' });
				else{
					next({status: 200, message: 'Error al borrar el dispositivo.'});
				}
			}
		});

	}
	
}


/*******
 * 
 * 	All users functions
 * 
 */

exports.getNotifications = function(req,res,next){

	var platform = req.params.platform || 'kasax';
	
	var Notification = require('../models/notification');
	/**
	* Get user notifications
	*
	* returns token
	**/
	var filter = {};
	var populate = [
		{path: '_user', select:'avatar firstname lastname'},
		{path: '_business_user', select:'avatar firstname lastname'}
    ];
	// Register business user device token
	if( req.user.business ) {
		filter._business_user_recipients = req.user._id;
	} else {
		filter._user_recipients = req.user._id;
	}
	filter.platform = platform;
	Notification.find(filter).select('title message params created_at _user _business_user').populate(populate).sort('-created_at').limit(50).exec(function(err, data){
		res.send({data: data});
	});
	
}



/*******
 * 
 * 	Get Exchange Rate
 * 
 */
exports.getExchangeRate = function(req,res,next) {
	
	common.getExchangeRate().then(
		data => {
			res.send(data);
		}
	).catch(
		err => {
			console.log(err);
			next({status: 200, message: 'Error al obtener el tipo de cambio.'});
		}
	);
    
}