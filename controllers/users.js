'use strict'

var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');
var UserPermission = require('../models/user_permission');
var UserRole = require('../models/user_role');
var getSlug = require('speakingurl');
var jwt = require('../services/jwt');
var mailing = require('../services/mailing');

var base_permissions = {
	'Users': {
		'Permissions': ['read','create','delete'],
		'Roles': ['read','create','update','delete'],
		'Users': ['read','create','update','delete']
	},
	'Settings': {
		'Catalogs': ['read','create','update','delete'],
		'Options': ['read','create','update','delete']
	},
	'Admin': {
		'Admin': ['admin']
	}
	
}

exports.generatePermissions = function(req,res){
	// Clear all permissions
	//UserPermission.remove({}, function(){});

	// foreach module key
	for(var module in base_permissions) {
	   	for(var submodule in base_permissions[module]) {
	   		base_permissions[module][submodule].forEach(function(item){
	   			var permission = [];
	   			permission.push(module);
	   			permission.push(submodule);
		   		permission.push(item);
		   		
		   		var new_data = new UserPermission();
				
				new_data.name = permission.join(' ');
				new_data.slug = getSlug(new_data.name);
				
				new_data.save( (err, permissionStored) => {
					if(err){
						console.log('error creating '+new_data.name+' permission');
					}
				} );

	   		});
	   	}
	}
	
	res.send(base_permissions);
}


/***************************************
*
*
*	Permisions function
*
*
***************************************/

exports.getPermissions = function(req,res){
	/**
	* Return a list of user permisions
	*
	* returns List
	**/
	UserPermission.find({}).sort({ name: 1 }).exec( (err, data) => {
		res.send({data: data});
	});
}

exports.addPermission = function(req,res,next){
	/**
	* Creates a new permission.
	*
	* slug Permission 
	* returns Permission
	**/

	var permission = new UserPermission();
	var params = req.body;
	if(!params.name)
		res.send({message: 'Field "name" is required'});
	permission.name = params.name;
	permission.slug = (params.slug) ? getSlug(params.slug) : getSlug(params.name);
	
	permission.save( (err, permissionStored) => {
		if(err){
			next({status: 200, message: 'Permission already exists.'});
		} else {
			if(permissionStored)
				res.send(permissionStored);
			else{
				next({status: 200, message: 'Error saving permission.'});
			}
		}
	} );
	
}


exports.deletePermissionById = function(req,res,next){
	/**
	* Delete permission.
	*
	* id Long ID of th permission to delete
	* no response value expected for this operation
	**/
	
	UserPermission.remove({_id:req.params.id}, (err, permissionRemoved) => {
		if(err){
			next({status: 200, message: 'Permission doesnt exists.'});
		} else {
			if(permissionRemoved)
				res.send(permissionRemoved);
			else{
				next({status: 200, message: 'Error deleting permission.'});
			}
		}
	});
	
}

/***************************************
*
*
*	Roles functions
*
*
***************************************/

exports.getRoles = function(req,res){
	/**
	* Return a list of user permisions
	*
	* returns List
	**/
	UserRole.find({}).exec(function(err, data){
		res.send({data: data});
	});
}

exports.addRole = function(req,res,next){
	/**
	* Creates a new permission.
	*
	* slug Permission 
	* returns Permission
	**/
	var role = new UserRole();
	var params = req.body;
	if(!params.name)
		res.send({message: 'Field "name" is required'});
	role.name = params.name;
	role.slug = (params.slug) ? getSlug(params.slug) : getSlug(params.name);
	role.permissions = params.permissions;

	role.save( (err, roleStored) => {
		if(err){
			next({status: 200, message: 'Role already exists.'});
		} else {
			if(roleStored)
				res.send(roleStored);
			else{
				next({status: 200, message: 'Error saving role.'});
			}
		}
	} );
}

exports.getRoleById = function(req,res,next){
	/**
	* Return a list of user roles
	*
	* id Long Id of the role to retrive information
	* returns Role
	**/
	UserRole.findById(req.params.id).exec((err, role) => {
		if(err){
			next({status: 200, message: 'Role doesnt exists.'});
		} else {
			if(role)
				res.send(role);
			else{
				next({status: 200, message: 'Error retriving role.'});
			}
		}
	});
	
}

exports.updateRoleById = function(req,res,next){
	/**
	* Update permission.
	*
	* id Long ID of the permission to update
	* returns Role
	**/
	var role = {};
	var params = req.body;
	if(!params.name)
		res.send({message: 'Field "name" is required'});
	role.name = params.name;
	role.slug = (params.slug) ? getSlug(params.slug) : getSlug(params.name);
	role.permissions = params.permissions;

	UserRole.findByIdAndUpdate(req.params.id, {$set: role}, {new: true} ).exec((err, role) => {
		if(err){
			next({status: 200, message: 'Role doesnt exists.'});
		} else {
			if(role)
				res.send(role);
			else{
				next({status: 200, message: 'Error updating role.'});
			}
		}
	});
	
}

exports.deleteRoleById = function(req,res,next){
	/**
   * Deleted permission.
   *
   * id Long ID of the permission to delete
   * returns Role
   **/
	UserRole.remove({_id:req.params.id}, (err, roleRemoved) => {
		if(err){
			next({status: 200, message: 'Role doesnt exists.'});
		} else {
			if(roleRemoved)
				res.send(roleRemoved);
			else{
				next({status: 200, message: 'Error deleting role.'});
			}
		}
	});
	
}



/***************************************
*
*
*	Users functions
*
*
***************************************/
exports.getUsers = function(req,res){
	/**
	* Return a list of SDR user
	*
	* returns User
	**/
	User.find({}).sort({ firstname: 1 }).exec(function(err, data){
		res.send({data: data});
	});
}

exports.searchUser = function(req,res,next){

	var terms = req.params.query.split(' ');

	var regexString = "";

	for (var i = 0; i < terms.length; i++)
	{
		regexString += terms[i];
		if (i < terms.length - 1) regexString += '|';
	}

	var re = new RegExp(regexString, 'ig');

	User.aggregate([
		{ $project : {
			fullname: {$concat: ['$firstname', ' ', '$lastname']},
			firstname: 1,
			lastname: 1,
			email: 1,
			keywe: 1,
			avatar: 1
			} 
		},
		{ $match : { $or:[ {fullname: re}, { email: {$regex : "^" + req.params.query} } ] } },
		{ $limit : 10 },
	])
	.exec()
	.then( users => {
		res.send({data: users});
	}).catch( err => {
		next({status: 500, message: 'Error searching users.'});
	});
	
}

exports.addUser = function(req,res,next){
	/**
	* Creates a new user.
	*
	* user User 
	* returns User
	**/
	var user = new User();
	var params = req.body;
	if(!params.firstname)
		res.send({message: 'Field "firstname" is required'});
	if(!params.lastname)
		res.send({message: 'Field "lastname" is required'});
	if(!params.email)
		res.send({message: 'Field "email" is required'});	
	user.firstname = params.firstname;
	user.lastname = params.lastname;
	user.email = params.email;
	user.password = bcrypt.hashSync(params.password);
	//user.password = bcrypt.hashSync(12345);
	user.role = params.role;
	user.extradata = params.extradata;
	user.secret_key = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 16);
	user.save( (err, userStored) => {
		if(err){
			next({status: 200, message: 'User already exists.'});
		} else {
			if(userStored)
			{

				// Send mail with token
				
				var token_encoded = jwt.createCustomToken({_user_id: userStored._id});

				var options = {
					subject: 'Bienvenido!',
					recipients: {},
					template: 'newUser'
				}
				options.recipients[userStored.email] = {
							'token': token_encoded,
							'name': userStored.firstname+' '+userStored.lastname
						};
				mailing.send(options);
				res.send(userStored);
			}
			else{
				next({status: 200, message: 'Error saving user.'});
			}
		}
	});
}

exports.getUserById = function(req,res,next){
	/**
	* Return user information
	*
	* id Long Id of the user to retrive information
	* returns User
	**/
	User.findById(req.params.id).exec((err, user) => {
		if(err){
			next({status: 200, message: 'User doesnt exists.'});
		} else {
			if(user)
				res.send(user);
			else{
				next({status: 200, message: 'Error retriving user.'});
			}
		}
	});
	
}

exports.updateUserById = function(req,res,next){
	/**
	* Delete SDR user.
	*
	* id Long ID of th SDR user to update
	* returns User
	**/
	var user = {};
	var params = req.body;
	if(!params.firstname)
		res.send({message: 'Field "firstname" is required'});
	if(!params.lastname)
		res.send({message: 'Field "lastname" is required'});
	if(!params.email)
		res.send({message: 'Field "email" is required'});
	user.firstname = params.firstname;
	user.lastname = params.lastname;
	user.email = params.email;
	user.extradata = params.extradata;
	user.role = params.role;
	//user.secret_key = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 16);
	

	User.findByIdAndUpdate(req.params.id, {$set: user}, {new: true} ).exec((err, user) => {
		if(err){
			next({status: 200, message: 'Error updating user.'});
		} else {
			if(user)
				res.send(user);
			else{
				next({status: 200, message: 'Error updating user.'});
			}
		}
	});
		
	
}


exports.deleteUserById = function(req,res,next){
	/**
	* Delete user.
	*
	* id Long ID of the SDR user to delete
	* returns User
	**/
	User.remove({_id:req.params.id}, (err, userRemoved) => {
		if(err){
			next({status: 200, message: 'User doesnt exists.'});
		} else {
			if(userRemoved)
				res.send(userRemoved);
			else{
				next({status: 200, message: 'Error deleting user.'});
			}
		}
	});
	
}
