'use strict'

var moment = require('moment');
var jwt = require('../services/jwt');
var config = require('config');
var User = require('../models/user');

exports.validate = function(req,res,next){
    
    var url = req.url;
    url = url.split('/');
    // return only parts of the url that aren't empty
    url = url.filter(function(el){ return el.length>0});
    // remove api version from the url
    if(url.length>0 && config.api_versions.indexOf(url[0])!=-1){
        url.shift();
    }
    

    // If the route is protected, then apply the middleware
    if('OPTIONS' != req.method && url.length>0 && config.public_directories.indexOf(url[0])==-1 && config.public_routes.indexOf(url.join('/'))==-1){
        
        // If auth header is not present, then return error 
        if(!req.headers.authorization){

            return res.status(403).send({message:'Auth header is required.', path: url });
        }
        // get token from header
        var token = req.headers.authorization.replace(/['"]+/g, '');
        try{
            // Decode token and validate is not expired
            var payload = jwt.decodeToken(token);
            if(!jwt.validateExpiration(payload.ext) || !jwt.validateKey(payload.secret_key)){
                return res.status(401).send({message:'Token expired.'});
            }
        } catch(ex) {
            // If token is not valid returns error
            return res.status(403).send({message:'Invalid token.'});
        }
            let user_populate = [
                {path : 'role', populate : {path : 'permissions', select : 'slug'}},
            ]
            User.findById(payload._id)
            .populate(user_populate)
            .exec( (err,userdata) => {
                if(err || !userdata){
                    return res.status(403).send({message:'User not authorized.'});
                }else {
                    req.user = userdata;
                    next();
                }                
                
            });
        
    } else {
        next();
    }
    
}