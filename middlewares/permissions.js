'use strict'

var jwt = require('../services/jwt');
var config = require('config');

// middleware for doing role-based permissions
exports.permit = function(permission) {
  return function(req, res, next) {
        if(!req.user || !req.user.role || !req.user.role.permissions){
            return res.status(403).send({message:'Unauthorized access. User not logged in.'});
        }
        var user_permissions = req.user.role.permissions.map( (el) => { return el.slug; } );
        if( user_permissions.indexOf(permission)==-1 ){
            return res.status(403).send({message:'Unauthorized access. ( '+permission+' is required)'});
        }
        next();
    }
}

exports.permitAny = function(permissions) {
    
  return function(req, res, next) {

        var auth = false;
        
        if(!req.user || !req.user.role || !req.user.role.permissions){
            return res.status(403).send({message:'Unauthorized access.'});
        }
        var user_permissions = req.user.role.permissions.map( (el) => { return el.slug; } );
        permissions.forEach( per => {
            if(!auth && user_permissions.indexOf(per)!==-1 ){
                auth = true;
                return next();
            }
        });
        if(!auth)
            return res.status(403).send({message:'Unauthorized access.', permissions: permissions});
        
    }
}