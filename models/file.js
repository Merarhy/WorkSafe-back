'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DirectorySchema = Schema({
    name: { type : String, required : true},
    favorite: { type : Boolean, required: false, default: false },
    type: {type: String, default: 'folder'},
    size: {type: String, default: '-'},
    created: {type: Date, default: Date.now }, 
});

var Directory = mongoose.model('Directory',DirectorySchema);


var FileSchema = Schema({
    name: { type : String, required : true},
    file: { type : String, required : true},
    mime: { type : String, required : true},
    type: {type: String, },
    size: {type: String },
    created: { type: Date, default: Date.now }
});

var Directory = mongoose.model('Directory',DirectorySchema);
var File = mongoose.model('File',FileSchema);



module.exports = {
  Directory: Directory,
  File: File,
 
}