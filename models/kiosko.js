'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var  kioskoSchema = Schema({
    name: { type : String, required : true },
    calle: { type : String, required : true },
    colonia: { type : String, required : true },
    municipio: { type : String, required : true },
    estado: { type : String, required : true },
    tel: { type : String, required : true },
    lat: { type : Number, required : true },
    lng: { type : Number, required : true },
    description: { type: String, required : true } ,
    img: { type: Schema.Types.Mixed },
    _categorie: { type: Schema.ObjectId, ref: 'Categorie' },
    created: { type: Date, default: Date.now }
});

var Kiosko = mongoose.model('Kiosko',kioskoSchema);



module.exports = {
    Kiosko: Kiosko
  }