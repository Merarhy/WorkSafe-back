'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');




var InfraestructuraSchema = Schema({
    temperatura: { type: Number, required: true},
    humedad:  { type: Number, required: true},
    energia: {  type: Number , required: true },
    aire: { type: Number , required: true },
    mant_ups: { type: Date },
    mant_plantas: { type: Date },
    mant_aire: { type: Date },
    date: { type: Date, required: true }
});

var Infraestructura = mongoose.model('Infraestructura', InfraestructuraSchema);


var TelecomunicationSchema = Schema({
  enlaces: { type: Number },
  pbx: { type: Number },
  date: { type: Date , default: Date.now }
});

var Telecomunication = mongoose.model('Telecomunication', TelecomunicationSchema);





module.exports = {
  Infraestructura: Infraestructura,
  Telecomunication : Telecomunication,

}