'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/**
 * 
 * Option
 * 
 */


var OptionSchema = Schema({
	slug: { type: String, required: true },
	data: { type: Schema.Types.Mixed },
	expireAt: { type: Date, default: undefined },
});

var Option = mongoose.model('Option',OptionSchema);

OptionSchema.index({ "expireAt": 1 }, { expireAfterSeconds: 0 });

/**
 * 
 * Ubicaciones
 * 
 */


var LocationSchema = Schema({
	name: { type: String, required: true },
	rooms: [String]
});

var Location = mongoose.model('Location',LocationSchema);

/**
 * 
 * Offices
 * 
 */


var OfficeSchema = Schema({
	name: { type: String, required: true },
	rooms: [String]
});

var Office = mongoose.model('Office',OfficeSchema);



var CategorieSchema = Schema({
	name: { type: String, required: true },
	icon: { type: String },
	order: {type: String }
});

var Categorie = mongoose.model('Categorie',CategorieSchema);




module.exports = {
  Option: Option,
  Location: Location,
  Office: Office,
  Categorie: Categorie
}