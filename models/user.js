'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
   
	firstname: { type : String, required : true},
	lastname: String,
	email: { type : String, unique : true, required : true},
	password: { type : String},
    avatar: { type : String},
	extradata: [{ type: Schema.Types.Mixed }],
	device_tokens: [{ type : String }],
	role: { type: Schema.ObjectId, ref: 'UserRole' },
	secret_key: { type : String, required : true},
	created_at: { type: Date, default: Date.now }

});

module.exports = mongoose.model('User',UserSchema);