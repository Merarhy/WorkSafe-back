'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserPermissionSchema = Schema({
	name: { type : String, required : true},
	slug: { type : String, unique : true, required : true}
});

module.exports = mongoose.model('UserPermission',UserPermissionSchema);