'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserRoleSchema = Schema({
	name: { type : String, required : true},
	slug: { type : String, unique : true, required : true},
	permissions: [{ type: Schema.ObjectId, ref: 'UserPermission' }]
});

module.exports = mongoose.model('UserRole',UserRoleSchema);