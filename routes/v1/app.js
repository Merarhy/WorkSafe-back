'use strict'

var express = require('express');
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var config = require('config');
var router = express.Router();

mongoose.connect('mongodb://'+config.mongo.url+':'+config.mongo.port+'/'+config.mongo.database, (err,res) => {
	if(err){
		throw err;
	} else {
		console.log('connected');
	}
});


/* GET home page. */
router.get('/', function(req, res, next) {
	res.status(200).send({message:'welcome to UTVT API'});
  //res.render('index', { title: 'Express mongo' });
});


//modules
require('./auth')(router);
require('./users')(router);
require('./files')(router);
require('./tools')(router);
require('./kiosko')(router);
/*
router.get('/usuarios/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
*/

module.exports = router;
