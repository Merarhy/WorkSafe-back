// Include controllers file
var AuthController = require('../../controllers/auth');

//initialize
module.exports = function (router) {
    
    //router.get('/test', AuthController.test );

    // Authentication
    router.post('/auth/login', AuthController.doLogin );
    router.get('/auth/validate-token', AuthController.validateToken );
    router.post('/auth/reset-password', AuthController.resetPassword );
    router.post('/auth/forgot-password', AuthController.forgotPassword );
    router.get('/auth/reset-password-request/:token', AuthController.resetPasswordRequest );
}
