// Include controllers file
var FileController = require('../../controllers/files');
var config = require('config');
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: config.upload_dir });
var access = require('../../middlewares/permissions');

//initialize
module.exports = function (router) {

    // Directories
    router.get('/business/:businessid/directories', access.permit('files-directories-read'), FileController.getDirectoriesByIdBusiness );
    router.post('/business/:businessid/directories', access.permit('files-directories-create'), FileController.addDirectoryByIdBusiness );
    router.delete('/business/:businessid/directories/:id', access.permit('files-directories-delete'), FileController.deleteDirectoryByIdBusinessById );
    router.patch('/business/:businessid/directories/:id/sdr_fav', access.permit('files-directories-sdrfav'), FileController.sdrfavDirectoryByIdBusinessById );
    router.patch('/business/:businessid/directories/:id/sdr_unfav', access.permit('files-directories-sdrfav'), FileController.sdrunfavDirectoryByIdBusinessById );
    


    // Files
    router.get('/business/:businessid/files', access.permit('files-files-read'), FileController.getBusinessFilesById );
    router.post('/business/:businessid/directories/:directoryid/files', [md_upload, access.permit('files-files-create')], FileController.addBusinessFile );
    router.delete('/business/:businessid/files/:file', access.permit('files-files-delete'), FileController.deleteBusinessFileById );
   // router.get('/files/:businessid/:file', access.permit('files-files-read'), FileController.serveBusinessFileById );
    router.get('/files/:businessid/:file', FileController.serveBusinessFileById );
    router.get('/avatars/:file', FileController.serveAvatarFileByName );

}

//app.use('/', routes);