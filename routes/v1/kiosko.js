var access = require('../../middlewares/permissions');
var KioskoController = require('../../controllers/kiosko');
var config = require('config');
var multipart = require('connect-multiparty');
var md_upload = multipart({ uploadDir: config.upload_site });
var access = require('../../middlewares/permissions');

module.exports = function (router) {

    // Get catalog fo Kiosko
    router.get('/kiosko/catalogos', KioskoController.getKioskoCatalogos);

    router.get('/catalogo/places/:catalogoid', KioskoController.getSitestoCatalogById);
    router.post('/catalogo/places/:catalogoid', [md_upload], KioskoController.addSitetoCatalogById);
    router.patch('/catolog/places/:siteid', [md_upload], KioskoController.editSiteToCatalogById);
    
    router.delete('/catalogo/places/:siteid', KioskoController.deleteSiteFromCatalog);


    router.get('/catalogo/site/:siteid', KioskoController.getSiteById);
    
    router.get('/site/img/:img', KioskoController.serverSiteImgId );

}