// Include controllers file

var ToolsController = require('../../controllers/tools');
var SettingsController = require('../../controllers/settings');
var access = require('../../middlewares/permissions');
//initialize
module.exports = function (router) {

    // Permissions generate
    router.get('/tools/test', ToolsController.test );

    // Collection manager
    router.get('/tools/catalog/:type/:collection', access.permitAny(['settings-catalogs-read']), ToolsController.getCollection );
    router.post('/tools/catalog/:type/:collection', access.permit('settings-catalogs-create'), ToolsController.addCollection );
    router.patch('/tools/catalog/:type/:collection/:id', access.permit('settings-catalogs-update'), ToolsController.updateCollectionById );
    router.delete('/tools/catalog/:type/:collection/:id', access.permit('settings-catalogs-delete'), ToolsController.deleteCollectionById );

    // Options Settings manager
    router.get('/tools/settings/options', access.permit('settings-options-read'), SettingsController.getOptions );
    router.post('/tools/settings/options', access.permit('settings-options-create'), SettingsController.addOption );
    router.patch('/tools/settings/options/:id', access.permit('settings-options-update'), SettingsController.updateOptionById );
    router.delete('/tools/settings/options/:id', access.permit('settings-options-delete'), SettingsController.deleteOptionById );
    
    // Mobile Device
    router.post('/tools/mobile/device-token/:token', ToolsController.registerDeviceToken );
    router.delete('/tools/mobile/device-token/:token', ToolsController.deleteDeviceToken );

    // Notifications for all users
    router.get('/tools/notifications', ToolsController.getNotifications );
    router.get('/tools/notifications/:platform', ToolsController.getNotifications );

    
}

//app.use('/', routes);