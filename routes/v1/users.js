
var UserController = require('../../controllers/users');
var access = require('../../middlewares/permissions');

module.exports = function (router) {
    // Permissions generate
    router.get('/tools/generate-permissions', UserController.generatePermissions );
    
     // Permissions
     router.get('/permissions', access.permit('users-permissions-read'), UserController.getPermissions );
     router.post('/permissions', access.permit('users-permissions-create'), UserController.addPermission );
     router.delete('/permissions/:id', access.permit('users-permissions-delete'), UserController.deletePermissionById );
 
     // Roles
     router.get('/roles', access.permit('users-roles-read'), UserController.getRoles );
     router.post('/roles', access.permit('users-roles-create'), UserController.addRole );
     router.get('/roles/:id', access.permit('users-roles-read'), UserController.getRoleById );
     router.patch('/roles/:id', access.permit('users-roles-update'), UserController.updateRoleById );
     router.delete('/roles/:id', access.permit('users-roles-delete'), UserController.deleteRoleById );
 
     // Users
     router.get('/users',  UserController.getUsers );
     router.post('/users', access.permit('users-users-create'), UserController.addUser );
     router.get('/users/:id', access.permit('users-users-read'), UserController.getUserById );
     router.patch('/users/:id', access.permit('users-users-update'), UserController.updateUserById );
     router.delete('/users/:id', access.permit('users-users-delete'), UserController.deleteUserById );
     //users email
     router.get('/users/search/:query', access.permitAny(['tools-users-search','users-users-read']), UserController.searchUser );
 

}