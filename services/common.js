'use strict'

exports.permitAny = function(permissions, req) {
    var auth = false;
    
    if(!req.user || !req.user.role || !req.user.role.permissions){
        return false;
    }
    var user_permissions = req.user.role.permissions.map( (el) => { return el.slug; } );
    permissions.forEach( per => {
        if(!auth && user_permissions.indexOf(per)!==-1 ){
            auth = true;
            return true;
        }
    });
    return auth;
}

exports._handleError = function(err){
	let errors = [];
    if(err){
        if(err.errors){
            for(var key in err.errors) {
                errors.push(err.errors[key].message);
            }
        } else {
            if(err.errmsg){
                errors.push(err.errmsg);
            } else {
                errors.push(err);
            }
        }
    }
    return errors;
}

exports.getUserWithPermission = function(user_permission){
	/**
	* Return a list of user with some permission
	*
	* returns Promise
	**/
	return new Promise( function(resolve, reject) {
		// First get keywe name of the location
		
		var User = require('../models/user');
        var all_promises = [];
        
        let populate_user = [
			{path: 'role', populate: {path: 'permissions'}}
		];
		
		all_promises.push( User.find({}).populate(populate_user).exec() );
		// Here we have locations and users
		
		Promise.all(all_promises).then( all_responses => {
			if (all_responses[0] && all_responses[0].length) {
				
				// Here we have all users
				var filtered_users = all_responses[0].filter( (el) => {
					// Map permissions into array
					var permissions = el.role.permissions.map( per => per.slug );
					// Return only users with selected permision
					return ( permissions.indexOf(user_permission) !== -1 );
				});

				resolve(filtered_users);
				
			} else {
                resolve([]);
            }
		}).catch( err => {
			console.log(err);
			// Return empty array if nolocation or user found, or any error ocurred
			reject([]);
		});
		
	});
	
}

exports.zeroPad = function (num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}

exports.addLaboralDays = function(days,some_date) {
    var Option = require("../models/settings").Option;
    var moment = require('moment');
    moment.locale('es');
    return new Promise(function(resolve, reject) {
        // Get timing options
        Option.find({},{'__v':0}).exec(function(err,options){
            var laboral_days = options.find( el => el.slug == 'laboral-days');
            var holidays = options.find( el => el.slug == 'holidays');
            /*console.log(laboral_days);
            console.log(holidays);*/
            var added_days = 0;
            while( added_days<days ) {
                some_date = moment(some_date).add(1,'day');
                // validate new date is in laboral days and is not a holiday
                if(holidays.data.indexOf( some_date.format('YYYY-MM-DD') ) == -1 && laboral_days.data.indexOf( some_date.day() ) !== -1 ) {
                    added_days++;
                }
            }
            resolve( some_date );
        })
    });
}

exports.getExchangeRate = function() {
    var Option = require("../models/settings").Option;
    var moment = require('moment');
    return new Promise(function(resolve, reject) {
        // Get cache or update
        Option.findOne({slug: 'exchange-rate'}).exec(function(err,exchangeRate){
            // If item is in cache, then return cache data
            if (exchangeRate) {
                resolve(exchangeRate);
            } else {
                // Make a new API call
                var https = require('https');
                var options = {
                    host: 'api.fixer.io',
                    port: 443,
                    path: '/latest?base=USD'
                };
                var output = '';
                https.get(options, function(response) {
                    response.on("data", function(chunk) {
                        output += chunk;
                    });
                    response.on("end", function() {
                        try{
                            var exchangeData = JSON.parse(output);

                            // Store into cache
                            var exchangeOption = new Option();
                            exchangeOption.slug = 'exchange-rate';
                            exchangeOption.data = exchangeData;
                            exchangeOption.expireAt = moment(new Date()).add(12,'h').toDate();
                            exchangeOption.save().then( storedCacheItem =>{
                                resolve(storedCacheItem);
                            }).catch( err => {
                                reject(err);
                            });
                        } catch(err){
                            reject(err)
                        }
                    });
                }).on('error', function(e) {
                    reject(e);
                });
            }
        });
    });
}