var crypto = require('crypto');
var config = require('config');
    
	algorithm = 'aes-256-ctr',
  password = 'd6F3Efeq';

exports.encrypt = function encrypt(text){
  var cipher = crypto.createCipher(config.mykio_crypto.algorithm,config.mykio_crypto.pass);
  var crypted = cipher.update(text,'utf8','hex');
  crypted += cipher.final('hex');
  return crypted;
}
 
exports.decrypt = function decrypt(text){
  var decipher = crypto.createDecipher(config.mykio_crypto.algorithm,config.mykio_crypto.pass);
  var dec = decipher.update(text,'hex','utf8');
  dec += decipher.final('utf8');
  return dec;
}
 
 exports.encrypt_buffer = function encrypt(buffer){
  var cipher = crypto.createCipher(config.mykio_crypto.algorithm,config.mykio_crypto.pass);
  var crypted = Buffer.concat([cipher.update(buffer),cipher.final()]);;
  return crypted;
}
 
exports.decrypt_buffer = function decrypt(buffer){
  var decipher = crypto.createDecipher(config.mykio_crypto.algorithm,config.mykio_crypto.pass);
  var dec = Buffer.concat([decipher.update(buffer) , decipher.final()]);;
  return dec;
}