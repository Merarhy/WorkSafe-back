'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('config');

exports.createToken = function(user){
    var payload = {
        _id: user._id,
        firstname: user.firstname,
        lastname: user.lastname,
        email: user.email,
        role: user.role,
        business: user.business,
        iat: moment().unix(),
        exp: moment().add(30,'days').unix()
    }

    return jwt.encode(payload,config.jwt.secret);
}

exports.decodeToken = function(token){
    var result = {};
    try {
        result = jwt.decode(token,config.jwt.secret);
    } catch (e) {
    }
    return result;
}

exports.createCustomToken = function(data){
    data.iat = moment().unix();
    if ( !data.exp )
        data.exp = moment().add(30,'days').unix();
    return jwt.encode(data,config.jwt.secret);
}

exports.validateExpiration = function(exp){
    if(exp <= moment().unix())
        return false;
    return true;
}

// IMPORTANT!!! - Complete function
exports.validateKey = function(secret_key){
    return true;
}