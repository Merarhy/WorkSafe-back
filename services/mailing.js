'use strict'

var config = require('config');
var mailgun = require('mailgun-js')({apiKey: config.mailgun.api_key, domain: config.mailgun.domain});
var fs = require('fs');
var path = require('path');

/**
* 
* 
*  Send mail via mailgun
*  params options = { recipients: {email:{var1:value1,var2:value2,...}}, subject: 'string', template: 'file template' }
* 
*/

/*
exports.example = function(req,res){
  var options = {
    subject: 'Welcome to KIO!',
    vars: {
      any: 'data'
    }
    recipients: {
      'lorddrak@gmail.com':{
        'data1': 'something',
        'name': 'Emmanuel Reyes T.'
      }
    },
    template: 'newUser'
  }
  mailing.send(options,function(response){
    res.send(response);
  });
}
*/

exports.send = function(options,callback){

    
    // Validate if recipients are received
    if(!options.recipients || Object.keys(options.recipients).length==0){
      if(callback)
        callback({status: 'error', message: 'Not recipient received'});
      return;
    }
    // Add email var to a vars lists
    for(var key in options.recipients) {
      options.recipients[key].email = key;
    }

    if(!options.template)
      options.template = config.mailgun.default_template;
    
    var template_file = path.join(__dirname, './'+config.mailgun.templates_dir+'/'+options.template+'.html');
    // readfile if exists
    if(fs.existsSync(template_file)){
      
      // Get template file content
      fs.readFile(template_file, 'utf8', function (err, htmlBody) {
        if(err){
          if(callback)
            callback({status: 'error', message: err});
        } else {

          // Replace Website URL
          while (htmlBody.indexOf('%website_url%') !== -1)
          {
              htmlBody = htmlBody.replace('%website_url%', config.website_url);
          }
          // Replace Website URL
          while (htmlBody.indexOf('%api_url%') !== -1)
          {
              htmlBody = htmlBody.replace('%api_url%', config.api_url);
          }
          // Replace Website URL
          while (htmlBody.indexOf('%current_api_version%') !== -1)
          {
              htmlBody = htmlBody.replace('%current_api_version%', config.current_api_version);
          }

          // Replace custom vars
          if(options.vars){
            for(var param in options.vars){
              while (htmlBody.indexOf('%vars.'+param+'%') !== -1) {
                  htmlBody = htmlBody.replace('%vars.'+param+'%', options.vars[param]);
              }   
            }
          }

          
          // Construct mail vars
          var data = {
            from: config.mailgun.from,
            to: Object.keys(options.recipients),
            subject: (typeof(options.subject)=='string') ? options.subject : config.mailgun.default_subject,
            html: htmlBody,
            "recipient-variables" : options.recipients
          };

          // Send mail
          mailgun.messages().send(data, function (error, body) {
            if(error){
              console.log( error );
              if(callback)
                callback({status: 'error', message: err});
            } else {
              if(callback)
                callback({status: 'success', data: body});
            }
          });
          
        }
      });
    } else {
      callback({status: 'error', message: 'Template not exists: '+template_file});
      return;
    }
    

}