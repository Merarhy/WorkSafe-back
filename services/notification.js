'use strict'


var User = require('../models/user');
var Notification = require('../models/notification');
var BusinessUser = require('../models/business_user');
var ExternalUser = require('../models/access').ExternalUser;
var config = require('config');
var mailing = require('./mailing');
var push = require('./push');




exports.send = function(options,cb){

    /***************
    // Options to create a
    let options = {
        pushOptions: {
            platform: 'default', // Platform name (kiolink, kasax, etc)
            title: 'string',    // Message title
            subtitle: 'string', // Message content
            tag: 'string',    // Message topic in order to group messages (default none)
            params: {
                data: {}, // custom data
                style: {} // phonegap push styles
            }  // Extra params ID
        },
        user: {}, // User or Business User Sender
        user_ids: [], // Array of user IDs that will receive the notification
        business_user_ids: [], // Array of business user IDs that will receive the notification
        external_user_ids: [], // Array of external user IDs that will receive the notification
        title: 'string',
        message: 'string',
        badge: number,
        extradata: { any: 'value'},
        personalized_extradata: { 
            user: { '_id': {
                    any: 'value
                }
            },
            business_user: { '_id': {
                    any: 'value
                }
            },
            external_user: { '_id': {
                    any: 'value
                }
            },
        },
        template: 'string'
    }
    ****************/

    return new Promise(function(resolve, reject) {
        
        // For KIO internal users
        var kio_users = [];
        if(options.user_ids)
            kio_users = User.find({'_id': { $in: options.user_ids }}).exec();
        
        // For Business users
        var business_users = [];
        if(options.business_user_ids)
            business_users = BusinessUser.find({'_id': { $in: options.business_user_ids }}).exec();
        // For Business users
        var external_users = [];
        if(options.business_user_ids)
            external_users = ExternalUser.find({'_id': { $in: options.external_user_ids }}).exec();
        
        // Get all promises data (all users retrived)
        Promise.all([kio_users,business_users,external_users]).then(proms => {

            // Create empty object for use in mailing service
            var mail_users = {};
            // Sort user data to fit email options
            proms[0].forEach( (el) => {
                mail_users[el.email] = {
                    email: el.email,
                    name: el.firstname+' '+el.lastname,
                    device_tokens: el.device_tokens
                }
                if(options.personalized_extradata && options.personalized_extradata.user[el._id]) {
                    Object.keys(options.personalized_extradata.user[el._id]).forEach(key => {
                        mail_users[el.email][key] = options.personalized_extradata.user[el._id][key];
                    });
                }
            });

            proms[1].forEach( (el) => {
                if( !mail_users[el.email] ) {
                    mail_users[el.email] = {
                        email: el.email,
                        name: el.firstname+' '+el.lastname,
                        device_tokens: el.device_tokens
                    }
                } else {
                    // Only concat device IDs if previously exists
                    mail_users[el.email].device_tokens = mail_users[el.email].device_tokens.concat(el.device_tokens)
                }
                if(options.personalized_extradata && options.personalized_extradata.business_user[el._id]) {
                    Object.keys(options.personalized_extradata.business_user[el._id]).forEach(key => {
                        mail_users[el.email][key] = options.personalized_extradata.business_user[el._id][key];
                    });
                }
            });

            proms[2].forEach( (el) => {
                if( !mail_users[el.email] ) {
                    mail_users[el.email] = {
                        email: el.email,
                        name: el.name,
                        device_tokens: [] // Set empty device tokens cause external don't use app
                    }
                } else {
                    // Only concat device IDs if previously exists
                    mail_users[el.email].device_tokens = mail_users[el.email].device_tokens.concat(el.device_tokens)
                }
                
                if(options.personalized_extradata && options.personalized_extradata.external_user[el._id]) {
                    Object.keys(options.personalized_extradata.external_user[el._id]).forEach(key => {
                        mail_users[el.email][key] = options.personalized_extradata.external_user[el._id][key];
                    });
                }
            });

            var params = {};
            for(var key in options.extradata){
                params[key] = options.extradata[key]
            }

            var mailOptions = {
                pushOptions: options.pushOptions,
                subject: options.title,
                vars: params,
                recipients: mail_users,
                template: options.template
            }
            
            // This var must contains additional notification params (options.pushOptions.params.data)
            
            // Store on the database
            var notificationDB = new Notification();
            notificationDB.title = options.pushOptions.title;
            notificationDB.message = options.pushOptions.subtitle;
            notificationDB.platform = options.pushOptions.platform || 'default';
            notificationDB.params = (options.pushOptions && options.pushOptions.params && options.pushOptions.params.data) ? options.pushOptions.params.data : {};
            notificationDB._user = (options.user && !options.user.business) ? options.user._id : undefined;
            notificationDB._business_user = (options.user && options.user.business) ? options.user._id : undefined;
            notificationDB._user_recipients = proms[0].map( (el) => {return el._id  } );
            notificationDB._business_user_recipients = proms[1].map( (el) => {return el._id  } );

            // Save notification on Data Base
            notificationDB.save( (error,notificationStored) => {
                // Send mail and then send push notifications
                mailing.send(mailOptions, function(mailResult){
                    //resolve(mailResult);
                    push.send(mailOptions, function( error, notifResult ) {
                        resolve({});
                    });

                });
            });
            
            
            
        }).catch( err => {
            console.log('error',err);
            reject(err);
        });
        



        
    });

}


// For push test notifications pourposes
exports.sendTest = function(device,cb){

    /***************
    // Options to create a
    let options = {
        title: 'string',
        message: 'string',
        bade: number,
        extradata: { any: 'value'}
    }
    ****************/


    // Search parent publication in order to get user owner
  
    let data = {
        title: 'Test', // REQUIRED 
        body: 'Test message body', // REQUIRED 
        topic: 'com.mx.future.kescucha'
    };
                        
    // You can use it in node callback style 
    push.send(device, data)
        .then((results) => {
            //console.log('results',results[0].message);
            cb(null,results);
        })
        .catch((err) => {
            //console.log('errors',err);
            cb(err)
        });
                        

}