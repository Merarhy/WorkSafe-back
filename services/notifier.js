'use strict'

var config = require('config');
var http = require('http');
var https = require('https');
var mailing = require('./mailing');

exports.send = function(options){
    // Only for testing
    return new Promise(function(resolve, reject) {
        /*
        sendSMS( options ).then( data => {
            resolve({ data: data });
        }).catch( err => {
            console.log('error', err);
            reject( { error: err } );
        });
        */
       resolve({});
    });

}

exports.makeCall = function( group ) {
    var id_grupo = group.id_group;
    var users = group.users.filter( el => el.phone );
    //console.log( users );
    
    return new Promise(function(resolve, reject) {

        var SSH = require('simple-ssh');
        var ssh = new SSH({
            port: config.pbx.port,
            host: config.pbx.host,
            user: config.pbx.username,
            pass: config.pbx.password
        });

        ssh.on('error', function(err) {
            console.log('Oops, something went wrong on SSH connection.');
            console.log(err);
            ssh.end();
            //reject( err );
        });

        // For each user in group
        users.forEach( function(usr, index) {
            console.log("execute ssh " + "perl /usr/local/bin/dial_asterisk_sare.pl "+transformRecordPhone(usr.phone)+" "+id_grupo+" MX");
            // Concat ssh requests in one SSH session
            ssh.exec("perl /usr/local/bin/dial_asterisk_sare.pl "+transformRecordPhone(usr.phone)+" "+id_grupo+" MX", {
                out: function(stdout) {
                    console.log('SSH executed');
                    console.log( stdout );
                    //resolve( stdout );
                }
            }).start();
            
        });

        // Finish SSH session
        setTimeout( () => {
            ssh.end();
        }, 500 * index);
        

    });
}

exports.makeRecord = function( group ) {
    
    var id_grupo = group.id_group;
    var users = group.users.filter( el => el.phone );
    
    console.log( users );
    
    return new Promise(function(resolve, reject) {

        
        console.log( users );

        users.forEach( function(usr, index) {
            setTimeout(() => {
                var SSH = require('simple-ssh');
                var ssh = new SSH({
                    port: config.pbx.port,
                    host: config.pbx.host,
                    user: config.pbx.username,
                    pass: config.pbx.password
                });

                ssh.on('error', function(err) {
                    console.log('Oops, something went wrong on SSH connection.');
                    console.log(err);
                    ssh.end();
                    //reject( err );
                });
                    
                console.log("execute ssh " + "perl /usr/local/bin/dial_alert_grab_sare.pl "+transformRecordPhone(usr.phone)+" "+id_grupo);
                ssh.exec("perl /usr/local/bin/dial_alert_grab_sare.pl "+transformRecordPhone(usr.phone)+" "+id_grupo, {
                    out: function(stdout) {
                        ssh.end();
                        console.log('SSH exec finished');
                        console.log( stdout );
                        //resolve( stdout );
                    }
                }).start();
            }, 500 * index);
        });
       
        

    });
}

exports.makeSms = function( group ) {
    
    if ( group && group.configsms && group.configsms.message_sms) {
        var id_grupo = group.id_group;
        var users = group.users.filter( el => el.phone );
        users = users.map( el => {
            el.phone = transformSmsPhone( el.phone );
            return el;
        });
        //console.log( users );
        
        return new Promise(function(resolve, reject) {

            var protocol = http;
            if(config.sms.ssl){
                protocol = https;
            }

            users.forEach( function(usr, index) {
                setTimeout(() => {
                    var path = config.sms.path;
                    path = path.replace("%user%",config.sms.user);
                    path = path.replace("%pass%",config.sms.pass);
                    path = path.replace("%text%",fixedEncodeURIComponent('Prueba: '+group.configsms.message_sms));
                    path = path.replace("%phone%",usr.phone);

                    var connection = {
                        host: config.sms.host,
                        port: config.sms.port,
                        path: path
                    };

                    // Make API call
                    var output = '';
                    console.log('Sending SMS to: ' + usr.phone);
                    protocol.get(connection, function(apiResponse) {
                        apiResponse.on("data", function(chunk) {
                            output += chunk;
                        });
                        // When API ends, manage data
                        apiResponse.on("end", function() {
                            // Transform XML to JSON
                            resolve(output);
                        });
                    }).on('error', function(e) {
                        console.log( 'error', e );
                        reject(e);
                    });
                }, 500 * index);
            });
        
        });
    }
    
}

exports.makeMail = function( group ) {
    return new Promise(function(resolve, reject) {

        
        var users = {};
        group.users.forEach( function(usr, index) {
            // Compose email and send
            users[ usr.email ] = {
                name: usr.firstname + ' ' +usr.lastname
            };
        });

        var subject = ( group.configemail && group.configemail.head ) ? group.configemail.head : 'Notificación de contingencia SARE'; 
        var message = ( group.configemail && group.configemail.message ) ? group.configemail.message : 'Por favor ponte en contacto con tu líder de quipo de trabajo';


        var options = {
            subject: 'Prueba: ' + subject,
            vars: {
                message: message
            },
            recipients: users,
            template: 'sareNotifier'
            }
        mailing.send(options,function(response){
            console.log( response );
        });
        

    });
}


function fixedEncodeURIComponent (str) {
    return encodeURIComponent(str).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
}

function transformRecordPhone( phone ) {
    // prepend external line for pbx
    var fixedPhone = config.pbx.external_line;
    // get only last 10 numbers
    phone = phone.substr(phone.length - 10);
    // validate is local or external number (compare has localcode 55)
    if ( phone.indexOf(config.pbx.localcode) === 0 ) {
        fixedPhone = fixedPhone + '044';
    } else {
        fixedPhone = fixedPhone + '045';
    }
    fixedPhone = fixedPhone + phone;
    return fixedPhone;
}

function transformSmsPhone( phone ) {
    // get only last 10 numbers
    return phone.substr(phone.length - 10);
}