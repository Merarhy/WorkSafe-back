'use strict'

var PushNotifications = new require('node-pushnotifications');
var config = require('config');
var moment = require('moment');

exports.send = function(options,callback){

    // Default push settings
    let pushOptions = {
        platform: 'default', // Platform name (kiolink, kasax, etc)
        title: 'KIO Networks',    // Message title
        subtitle: 'Hay nueva actividad en KIO, ¿por qué no echas un vistazo?', // Message content
        tag: 'kio',    // Message topic in order to group messages (default none)
        params: {}  // Extra params ID
    };

    // Override setting values with the received data
    if( options.pushOptions ) {
        for(var key in options.pushOptions){
            pushOptions[key] = options.pushOptions[key]
        }
    }

    // Setting default push notification style
    pushOptions.params = {
        notId: moment().format('HHmmss'), // Retrive or set default notId
        title: pushOptions.title,
        body: pushOptions.subtitle,
        tag: pushOptions.tag,
        vibrationPattern: config.firebase[pushOptions.platform].options.vibrate,
        visibility: 1,
        custom: {}
    }

    if( options.pushOptions ) {
        for(var key in options.pushOptions){
            if ( key != 'params' ) {
                pushOptions[key] = options.pushOptions[key];
            }
        }
    }

    // Override custom data
    if( options.pushOptions && options.pushOptions.params && options.pushOptions.params.data ) {
        pushOptions.params.data = options.pushOptions.params.data
    }

    // Override push notification styles and add custom data
    if( options.pushOptions && options.pushOptions.params && options.pushOptions.params.style ) {
        for(var key in options.pushOptions.params.style ){
            pushOptions.params[key] = options.pushOptions.params.style[key]
        }
    }
    if ( options.pushOptions.params.data ) {
        pushOptions.params.custom = options.pushOptions.params.data;
    }

    const settings = {
		gcm: {
			id: config.firebase[pushOptions.platform].gcm.id,
			phonegap: config.firebase[pushOptions.platform].gcm.phonegap
		},
		apn: {
			token: {
                key: config.firebase[pushOptions.platform].apn.token.key, // optionally: fs.readFileSync('./certs/key.p8') 
                keyId: config.firebase[pushOptions.platform].apn.token.keyId,
                teamId: config.firebase[pushOptions.platform].apn.token.teamId
			},
			production: config.firebase[pushOptions.platform].apn.production
		}
    };
	
	const push = new PushNotifications(settings);

    // Multiple destinations 
    var registrationIds = [];
    for(var key in options.recipients) {
        registrationIds = registrationIds.concat(options.recipients[key].device_tokens);
    }

    //pushOptions.params.notId = notId;
    
    //pushOptions.params.style = "inbox";
    
    //pushOptions.params.style = "picture";
    //pushOptions.params.picture = 'http://36.media.tumblr.com/c066cc2238103856c9ac506faa6f3bc2/tumblr_nmstmqtuo81tssmyno1_1280.jpg';
    
    //pushOptions.params.image = "http://36.media.tumblr.com/c066cc2238103856c9ac506faa6f3bc2/tumblr_nmstmqtuo81tssmyno1_1280.jpg";
    //pushOptions.params['image-type'] = "circle";
    /*
    pushOptions.params.actions = [
        { "icon": "emailGuests", "title": "Reply", "callback": "emailGuests", "foreground": false, "inline": true, "replyLabel": "Enter your reply here" },
        { "icon": "snooze", "title": "SNOOZE", "callback": "snooze", "foreground": false}
    ];*/
    
	let data = {
        title: pushOptions.title,
		body: pushOptions.subtitle, // REQUIRED 
		custom: pushOptions.params,
        contentAvailable: true,
		icon: config.firebase[pushOptions.platform].options.icon, // gcm for android 
		topic: config.firebase[pushOptions.platform].options.topic,
		color: config.firebase[pushOptions.platform].options.iconColor,
		tag: pushOptions.tag, // Change in order to make different notifications
        priority: 'high',
        sound: config.firebase[pushOptions.platform].options.sound,
        //category: 'kasax'
        
    }
    //console.log(settings);
    //console.log(registrationIds);

    // Clean only existing items
    registrationIds = registrationIds.filter( el => {
        if ( el )
            return el;
    });
    
	// Or you could use it as a promise: 
	push.send(registrationIds, data)
    .then(	(results) => {
        //console.log('success', push);
        callback(results);
    })
    .catch((err) => {
        //console.log('error', err);
        callback({}, err);
    });


    

}