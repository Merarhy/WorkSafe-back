'use strict'
var fs = require('fs');
var path = require('path');
var csv = require('fast-csv');
var SalesforceProduct = require('../models/infra').SalesforceProduct;
var Business = require('../models/business');
var common = require('./common');

/* Call this from the controller */
/*
csv.read_salesforce_products('./catalogs/salesforce_products_with_codes.csv',function(response){
    res.send({data:response});
});
*/

exports.read_salesforce_products = function(filename,cb){

    SalesforceProduct.remove({}, function(){
        var file = path.join(__dirname, filename);
        var stream = fs.createReadStream(file, {encoding: "utf8"});
        var tmp_items = [];
        var csvStream = csv()
            .on("data", function(data){
                tmp_items.push(insertSalesforceProduct(data));
            })
            .on("end", function(data){
                cb(tmp_items)
            });
        stream.pipe(csvStream);
    });
	
}

function insertSalesforceProduct(product){
    var sp = new SalesforceProduct();
    sp.id_salesforce = product[0];
    sp.nombre = product[1];
    //sp.familia = product[2];
    sp.familia = 'Data Center Services';
    sp.descripcion = '';
    sp.location = product[2];
    sp.producto = product[4];
    sp.cve_producto = common.get_product_cve(product[4]);
    sp.plazo_mayor = (product[3]=='> a 24 meses') ? true : false;
    sp.nrc = product[6];
    sp.mrc = product[7];
    sp.costo = product[8];
    sp.precio = product[9];
    sp.save();
    return sp;
}


/*********
 * 
 * 
 *  Load all business from CSV
 * 
 * 
 */
/* Call this from the controller */
/*
csv.load_salesforce_business('./catalogs/empresas_salesforce.csv').then( data => {
    res.send({data:data});
}).catch( err => {
    res.send({data: err});
});
*/

exports.load_salesforce_business = function(filename, clear_business ){

    return new Promise(function(resolve, reject) {
        var Business = require('../models/business');
        var BusinessConsole = require('../models/business_console');
        var BusinessDevice = require('../models/business_device');
        var BusinessUserDatacenterRequest = require('../models/business_user_extra').BusinessUserDatacenterRequest;
        var BusinessUser = require('../models/business_user');
        var Directory = require("../models/file").Directory;
        var Files = require("../models/file").File;
    
    
        var delete_all = [];
        if( clear_business ) {
            delete_all.push( Business.remove({}) );
            delete_all.push( BusinessConsole.remove({}) );
            delete_all.push( BusinessDevice.remove({}) );
            delete_all.push( BusinessUserDatacenterRequest.remove({}) );
            delete_all.push( BusinessUser.remove({}) );
            delete_all.push( Directory.remove({}) );
            delete_all.push( Files.remove({}) );
        }
    
        Promise.all(delete_all).then(
            data => {
                read_file_promise(filename).then(
                    data => {
                        var tmp_items = [];
                        data.forEach( el => {
                            tmp_items.push(insertSalesforceBusiness(el));
                        });
                        Promise.all(tmp_items).then(
                            inserted => {
                                resolve(inserted);
                            }
                        ).catch(
                            err => {
                                reject(err);
                            }
                        );
                    }
                ).catch(
                    err => {
                        reject( err );
                    }
                );
            }
        ).catch(
            err => {
                reject( err );
            }
        );
    });


    
}

function insertSalesforceBusiness(item){
    var bus = {};
    bus.name = item[9];
    bus.normalized_name = item[9].toLowerCase();
    bus.id_salesforce = item[8];
    bus.pais = item[19];
    bus.estado = item[20];
    bus.extradata = [
        { clave: 'origin', valor: 'DCS'},
        { clave: 'division', valor: item[0]},
        { clave: 'director_division', valor: item[1]},
        { clave: 'empresa', valor: item[2]},
        { clave: 'gerencia', valor: item[3]},
        { clave: 'id_propietario', valor: item[4]},
        { clave: 'propietario', valor: item[5]},
        { clave: 'service_manager', valor: item[6]},
        { clave: 'service_delivery', valor: item[7]},
        { clave: 'num_sap', valor: item[10]},
        { clave: 'sociedad', valor: item[11]},
        { clave: 'service_op_mngr', valor: item[12]},
        { clave: 'ejecutivo_csc', valor: item[13]},
        { clave: 'sector', valor: item[14]},
        { clave: 'clasificacion', valor: item[15]},
        { clave: 'razon_social', valor: [item[16]]},
        { clave: 'num_ident_fiscal', valor: item[18]},
        { clave: 'website', valor: item[21]},
        { clave: 'carrier', valor: (item[22]=='SI') ? true : false},
        { clave: 'interno', valor: (item[23]=='SI') ? true : false}
    ]
    return Business.update({id_salesforce:item[8]}, bus, {upsert: true, setDefaultsOnInsert: true});
}


/* Call this from the controller */
/*
var csv = require('../services/read-csv');

var filenames = {
    business: './catalogs/empresas.csv',
    nimsoft_spectrum: './catalogs/nimsoft_spectrum.csv',
    servicenow: './catalogs/servicenow.csv'
}
csv.read_business_consoles(filenames).then( data => {
    res.send({data:data});
}).catch( err => {
    res.send({data: err});
})
*/


/* EXAMPLE filenames VAR
filnames = {
    business: 'business.csv',
    cruce: 'cruce.csv',
    nimsoft_spectrum: 'consoles1.csv',
    servicenow: 'consoles2.csv'
}
*/
exports.read_business_consoles = function(filenames){
    // Require models
    var BusinessUser = require('../models/business_user');
    var BusinessConsole = require('../models/business_console');
    var Business = require('../models/business');
    var BusinessDevice = require('../models/business_device');
    var Directory = require("../models/file").Directory;
    var Files = require("../models/file").File;

    // Return a promise
    return new Promise(function(resolve, reject) {
        var promises = [];
        // Add business promise
        promises.push( read_file_promise(filenames.business) );
        promises.push( read_file_promise(filenames.cruce) );
        promises.push( read_file_promise(filenames.nimsoft_spectrum) );
        promises.push( read_file_promise(filenames.servicenow) );
        
        promises.push( Business.find().exec() );

        // When all promises are complete
        Promise.all(promises).then(
            data => {
                // 1ST STEP - CREATE NON EXISTING BUSINESS (DOESNT UPDATE OLD ONES)
                var update_business_array = [];
                var current_id_salesforce = [];
                    // Filter repeated items
                var new_business = data[0].filter( el => {
                    if(current_id_salesforce.indexOf(el[3]) == -1 ) {
                        current_id_salesforce.push( el[3] );
                        return true;
                    } else {
                        return false;
                    }
                });
                    // Filter existing DCS items
                var old_id_salesforce = data[4].filter(el => {
                    var dcs = el.extradata.find( ext => {
                        return ext.clave == 'origin' && ext.valor == 'DCS';
                    });
                    return dcs;
                }).map( el => el.id_salesforce );
                
                new_business = new_business.filter( el => old_id_salesforce.indexOf(el[3])==-1 );
                
                new_business.forEach( el => {
                    var bus = {};
                    bus.name = el[5];
                    bus.normalized_name = el[5].toLowerCase();
                    bus.id_salesforce = el[3];
                    bus.extradata = [
                        { clave: 'origin', valor: 'MS'},
                        { clave: 'division', valor: el[0]},
                        { clave: 'director_division', valor: el[1]},
                        { clave: 'empresa', valor: ''},
                        { clave: 'gerencia', valor: ''},
                        { clave: 'id_propietario', valor: ''},
                        { clave: 'propietario', valor: el[2]},
                        { clave: 'service_manager', valor: ''},
                        { clave: 'service_delivery', valor: ''},
                        { clave: 'num_sap', valor: el[3]},
                        { clave: 'sociedad', valor: el[9]},
                        { clave: 'service_op_mngr', valor: ''},
                        { clave: 'ejecutivo_csc', valor: ''},
                        { clave: 'sector', valor: ''},
                        { clave: 'clasificacion', valor: ''},
                        { clave: 'razon_social', valor: [el[6]]},
                        { clave: 'num_ident_fiscal', valor: el[8]},
                        { clave: 'website', valor: ''},
                        { clave: 'carrier', valor: false},
                        { clave: 'interno', valor: false}
                    ]
                    update_business_array.push( Business.update({id_salesforce:el[3]}, bus, {upsert: true, setDefaultsOnInsert: true}) );
                });
                // When all business has been updated
                Promise.all( update_business_array ).then(
                    inserted => {
                        // Retrive business keys, filtered by unique id_salesforce and initialize each console
                        current_id_salesforce = [];
                        var current_business = {};
                        var current_business_keys = {};
                        data[1].filter( el => {
                            if( el[5] && current_id_salesforce.indexOf(el[5]) == -1  ) {
                                current_id_salesforce.push( el[5] );
                                return true;
                            } else {
                                return false;
                            }
                        }).forEach( el => {
                            // Store Keyname by id_salesforce
                            current_business_keys[el[1]] = el[5];
                            // initialize consoles
                            current_business[el[5]] = {
                                nimsoft: [],
                                spectrum: [],
                                servicenow: []
                            }
                        })

                        // Retrive nimsoft and spectrum consoles
                        data[2].forEach( con => {
                            // Validate business exists
                            if ( con[1] && current_business[con[1]] ) {
                                if ( con[3].toLowerCase().indexOf('spectrum') !== -1 ){
                                    current_business[con[1]].spectrum.push( con[2] );
                                } else {
                                    current_business[con[1]].nimsoft.push( con[2] );
                                }    
                            }
                        });
                        // Retrive servicenow consoles
                        data[3].forEach( con => {
                            // Validate business exists
                            if ( con[1] && current_business[con[1]] ) {
                                current_business[con[1]].servicenow.push( con[2] );
                            } else {
                                //if(con[1]){
                                //    console.log('Con llave: '+con[1]);
                                //} else {
                                //    console.log('Sin llave '+con[2]);
                                //}
                            }
                            
                        });

                        // Retrive again all updated business
                        Business.find().exec().then(
                            all_business => {
                                // Update business consoles on DB
                                all_business.forEach( el => {
                                    if(current_business_keys[el.id_salesforce]){
                                        current_business[current_business_keys[el.id_salesforce]]._id = el._id
                                    }
                                });
                                // Remove all previous consoles
                                BusinessConsole.remove({}).then(
                                    rem_console => {
                                        var all_promises = [];
                                        Object.keys(current_business).forEach( bus_key => {
                                            if( current_business[bus_key].nimsoft.length ) {
                                                current_business[bus_key].nimsoft.forEach( curr_bus_cons => {
                                                    var consol_nimsoft = new BusinessConsole();
                                                    consol_nimsoft.type = 'nimsoft';
                                                    consol_nimsoft.data = {
                                                        name: curr_bus_cons
                                                    };
                                                    consol_nimsoft.business = current_business[bus_key]._id;
                                                    all_promises.push( consol_nimsoft.save() );
                                                });
                                            }
                                            
                                            if( current_business[bus_key].spectrum.length ) {
                                                current_business[bus_key].spectrum.forEach( curr_bus_cons => {
                                                    var consol_spectrum = new BusinessConsole();
                                                    consol_spectrum.type = 'spectrum';
                                                    consol_spectrum.data = {
                                                        name: curr_bus_cons
                                                    };
                                                    consol_spectrum.business = current_business[bus_key]._id;
                                                    all_promises.push( consol_spectrum.save() );
                                                });
                                            }
                                            if( current_business[bus_key].servicenow.length ) {
                                                current_business[bus_key].servicenow.forEach( curr_bus_cons => {
                                                    var consol_servicenow = new BusinessConsole();
                                                    consol_servicenow.type = 'servicenow';
                                                    consol_servicenow.data = {
                                                        name: curr_bus_cons
                                                    };
                                                    consol_servicenow.business = current_business[bus_key]._id;
                                                    all_promises.push( consol_servicenow.save() );
                                                });
                                            }
                                            
                                        });
                                        Promise.all(all_promises).then(
                                            updated_consoles => {
                                                resolve({
                                                    business: all_business.length,
                                                    consoles: updated_consoles.length
                                                });
                                            }
                                        ).catch(
                                            err => {
                                                reject( err );
                                            }
                                        );
                                    }
                                ).catch(
                                    err => {
                                        reject( err );
                                    }
                                );
                            }
                        ).catch(
                            err => {
                                reject( err );
                            }
                        );
                        
                    }
                ).catch(
                    err => {
                        reject( err );
                    }
                );
                
                
                ////////// insert keyname




                
                
                /*********
                 * 
                 * 
                 * Start Old Method
                
                
                // Insert business and their consoles
                var total_business = Object.keys(business).length;
                var all_promises = [];
                Object.keys(business).forEach( bus_name => {
                    if ( bus_name && bus_name.length ) {
                        var bus = business[bus_name];
                        var new_business = new Business();
                        new_business.name = bus.name;
                        new_business.normalized_name = bus.name.toLowerCase()
                        new_business.extradata = bus.extradata;
                        all_promises.push( new_business.save() );
                        if( bus.consoles.nimsoft.length ) {
                            bus.consoles.nimsoft.forEach( curr_bus_cons => {
                                var consol_nimsoft = new BusinessConsole();
                                consol_nimsoft.type = 'nimsoft';
                                consol_nimsoft.data = {
                                    name: curr_bus_cons
                                };
                                consol_nimsoft.business = new_business._id;
                                all_promises.push( consol_nimsoft.save() );
                            });
                        }
                        if( bus.consoles.spectrum.length ) {
                            bus.consoles.spectrum.forEach( curr_bus_cons => {
                                var consol_spectrum = new BusinessConsole();
                                consol_spectrum.type = 'spectrum';
                                consol_spectrum.data = {
                                    name: curr_bus_cons
                                };
                                consol_spectrum.business = new_business._id;
                                all_promises.push( consol_spectrum.save() );
                            });
                        }
                        if( bus.consoles.servicenow.length ) {
                            bus.consoles.servicenow.forEach( curr_bus_cons => {
                                var consol_servicenow = new BusinessConsole();
                                consol_servicenow.type = 'servicenow';
                                consol_servicenow.data = {
                                    name: curr_bus_cons
                                };
                                consol_servicenow.business = new_business._id;
                                all_promises.push( consol_servicenow.save() );
                            });
                        }
                    }
                });
                
                Promise.all(all_promises).then( end => {
                    resolve(business);
                }).catch( err => {
                    reject(err);
                });
                *
                *
                *   END OLD METHOD
                *
                *******/
                
            }).catch(
            err => {
                console.log('reject', err);
                reject(err);
            }
        );
    });
    
}


function read_file_promise( filename ){
    return new Promise(function(resolve, reject) {
        var file = path.join(__dirname, filename);
        if (fs.existsSync(file)) { 
            var stream = fs.createReadStream(file, {encoding: "utf8"});
            var tmp_items = [];
            var csvStream = csv()
                .on("data", function(data){
                    tmp_items.push(data);
                })
                .on("end", function(data){
                    // Here we have all business data organized, now we need to read all consoles data
                    resolve(tmp_items);
                });
            stream.pipe(csvStream);
        } else {
            reject( 'File '+file+' not found' );
        }
    });
}